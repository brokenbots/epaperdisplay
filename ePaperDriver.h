/* ePaperDisplay.h */
#include <Arduino.h>

clase ePaperDisplay
{
	public:
		// Constructor
		ePaperDisplay(int EIO1, int XCK, int LATCH, int SLEEPB, int DI0);
	
		/* 
	 	writeCharDisplay()
	 	takes two char[10] arrays, and the background option
	 	arrays much be char datatypes, so number data will need to be converted first
	 	setting whiteBG to true will invert the display
		*/
		void writeCharDisplay(char *lineA, char *lineB);
	
		/*
		writeRawDisplay()
		takes two integer arrays of 10, each int object is a bit map  
		*/
		void writeRawDisplay(int *lineA, int *lineB)
		
    	/*
	 	testPattern()
	 	just displays a test pattern of all segments turned on against black bg
	 	or optionally set whiteBG to invert the display
		*/
		void testPattern();
	
		/*
		character map, you need to be able to override this, just give it your own
		a 128 element integer array, again each integer is a bit map
		*/
		void setCharMap( int *charMap);
		
		// setting this should invert the display
		bool invertDisplay;
		
		/*
	 	setting will make addressing precisely follow the layout in the datasheet
		in other words, dataRow1 will be upside down
		*/
		bool directAccessMode;
		
		// This sets the first line of the display
		int dataRowA[10];
		
		// This sets the second line of the display
		int dataRowB[10];
		
	private:
		// move the clock forward one
		void clock();

		// this is should loop through the display array data and map it to it's segments, called for each line
		// charInvert will flip the characters on a line, useful for the dataRowA
		void segmentLineMapper(int *displayLine, bool charInvert);
		
		// write the data to the dislay
		void write();
		
		// default character map
		int dfltCharMap[128];
		
		// Pin Assignment
		int _EI0;
		int _XCK;
		int _LATCH;
		int _SLEEPB;
		int _DI0;

}