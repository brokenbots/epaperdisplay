/* ePaperDisplay.cc

David Sanderson dave(at)brokenbots.net
License: MIT

This is an ePaper Display Library for the 16 segment 20 character display sold at Sparkfun.com
the library is based in part off the original sample code by Jim Linblum at Sparkfun and the some
of the Library from bldr.com.  However it contains a larger set of characters and the display data
is kept in a 128 int array.  Each member of the array is a hex value that corresponds to a the letter
that would be in that asci position eg. A = 66 = char_array[66] = 0x01CD.  This reduces the memory requirements
and removes the case statements, it also should make it easier to modify characters, maybe.
The pin will inturn be pulled high or low by reading through each integer value with bitRead(), 0 LOW, 1 HIGH.

*/

#include <Arduino.h>
#include <ePaperDisplay.h>

// the charater mapping is the in the charater_map.txt file
int dfltCharMap[126] = {
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, // 0-7:     nul, nul, nul, nul, nul, nul, nul, nul,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, // 8-15:    nul, tab,  lf, nul,  cr, nul, nul, nul,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, // 16-23:   nul, nul, nul, nul, nul, nul, nul, nul,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, // 24-31:   nul, nul, nul, nul, nul, nul, nul, nul,
	0x0000, 0x0000, 0x0110, 0xFFFF, 0x9BC9, 0x5BCA, 0x93DC, 0x0100, // 32-39:   spc,   !,   ",   #,   $,   %,   &,   ',
	0x0000, 0x0000, 0x66AA, 0x1188, 0x0002, 0x0010, 0x0005, 0x4002, // 40-47:     (,   ),   *,   +,   ,,   -,   .,   /,
	0xAA55, 0x2800, 0xB24D, 0xBA41, 0x3818, 0x8659, 0x9A5D, 0xA840, // 48-55:     0,   1,   2,   3,   4,   5,   6,   7,
	0xBA5D, 0xBA59, 0x0000, 0x0000, 0x4400, 0x0009, 0x0022, 0x014C, // 56-63:     8,   9,   :,   ;,   <,   =,   >,   ?,
	0x0000, 0xB85C, 0xBBC1, 0x8255, 0xABC1, 0x825D, 0x805C, 0x9A55, // 64-71:     @,   A,   B,   C,   D,   E,   F,   G,
	0x381C, 0x83C1, 0x81C5, 0x441C, 0x0215, 0x6834, 0x2C34, 0xAA55, // 72-79:     H,   I,   J,   K,   L,   M,   N,   O,
	0xB05C, 0xAE55, 0xB45C, 0x9A61, 0x81C0, 0x2A15, 0x4016, 0x2C16, // 80-87:     P,   Q,   R,   S,   T,   U,   V,   W,
	0x4422, 0x40A0, 0xC243, 0x8380, 0x0420, 0x01C1, 0x0030, 0x0001, // 88-95:     X,   Y,   Z,   [,   \,   ],   ^,   _,
	0x0020, 0x01CD, 0x009D, 0x000D, 0x018D, 0x015D, 0x9188, 0x01D9, // 96-103:    `,   a,   b,   c,   d,   e,   f,   g, 
	0x009C, 0x0004, 0x0181, 0x4580, 0x0014, 0x188C, 0x008C, 0x008D, // 104-111:   h,   i,   j,   k,   l,   m,   n,   o,
	0x015C, 0x0398, 0x000C, 0x00D9, 0x001D, 0x0181, 0x0006, 0xC00A, // 112-119:   p,   q,   r,   s,   t,   u,   v,   w, 
	0x4422, 0x0199, 0x000B, 0x0000, 0x0180, 0x0000, 0xF398, 0x0000  // 120-127:   x,   y,   z,   {,   |,   },   ~, nul
};


// character arrays 1  2  3  4  5  6  7  8  9 10
// row 1
int dataRowA[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
// row 2
int dataRowB[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// Display invert control variable
bool invertDisplay = false;
// Display access mode
bool directAccessMode = false;

// Constructor
ePaperDisplay::ePaperDisplay(int EIO, int XCK, int LATCH, int SLPB, int DI0) 
{
	// set pin values
	int _EIO = EIO;
	int _XCK = XCK;
	int _LATCH = LATCH;
	int _SLPB = SLBP;
	int _DI0 = DI0;
	
	// set all pins to output
	pinMode(_EIO, OUTPUT);
	pinMode(_XCK, OUTPUT);
	pinMode(_LATCH, OUTPUT);
	pinMode(_SLPB, OUTPUT);
	pinMode(_DI0, OUTPUT);
	
	// set all pins to low, lets save some battery
	digitalWrite(_SLPB, LOW);      // Sleep high turns the display on, so we'll only pull it when we're refreshing the display
	digitalWrite(_DI0, LOW);       // Initialize data high, again only when we're ready to refresh the display
	digitalWrite(_XCK, LOW);        
	digitalWrite(_EIO, HIGH);
	digitalWrite(_LATCH, LOW);
}

// Moves the clock forward
ePaperDisplay::clock()
{
	digitalWrite(_XCK, HIGH);
	delayMicroseconds(1);
	digitalWrite(_XCK, LOW);
	delayMicroseconds(1);
}

ePaperDisplay::segmentLineMapper( int *displayLine, bool invert)
for (int c=0, c < 10, c++)
{
	for (int s=0, s < 16, s++)
	{
		if ( binaryRead(displayLine[i][s]) == 0 ) 
		{
			digitalWrite(_DI0, LOW);
		} else {
			digitalWrite(_DI0, HIGH);
		}
	}
	delayMicroseconds(1);
	clock();
}

/* 
Write display data, this takes our row based bit maps and aggregates our setting and then sends data to the display
*/
ePaperDisplay::write()
{

	// first lets wake up the screen and get it ready for data
	digitalWrite(_SLPB, HIGH);      // Sleep high turns the display on
  	digitalWrite(_DI0, HIGH);       // Initialize data high

	// WRITE ROW A DATA TO MEMORY //
	// set the background color, HIGH for dark, LOW for light, or default is dark
	if (invertDisplay = true)  // Y0 EIO1
	{
		digitalWrite(_DI0, LOW);
	} else {
		digitalWrite(_DI0, HIGH);
	}	
	delayMicroseconds(1);
	clock();
	// this will actually map the segments to rowA, it will also flip the data, make it readable
	segmentLineMapper( dataMapA, true);
	// Send the com bit, not sure what this does at this point.
	digitalWrite(_DI0, HIGH);  // Y161
	clock();
	
	// WRITE ROW B DATA TO MEMORY //
	// repeat what we just did
	if (invertDisplay = true) // Y0 EIO2
	{
		digitalWrite(_DI0, LOW);
	} else {
		digitalWrite(_DI0, HIGH);	
	}
	delayMicroseconds(1);
	clock();
	// this will map the segments to rowB, we don't flip the bits on this data
	segmentLineMapper( dataMapB, true);
	// again not sure what this bit does
	digitalWrite(_DIO,HIGH);
	clock();
	
	// Pull the latch!! Transfers the data from display memory to the physical display
	// looking over the docs, we should do this twice, HIGH, LOW, HIGH, LOW type of thing
	digitalWrite(_LATCH, HIGH);
	delayMicroseconds(5);
	digitalWrite(_LATCH, LOW);
	delayMicroseconds(5);
	digitalWrite(_LATCH, HIGH);
	delayMicroseconds(5);
	digitalWrite(_LATCH, LOW);
	delayMicroseconds(5);
	
	// Now that the data is on the display, lets put it back to the initial state
	digitalWrite(_SLPB, LOW);      // Sleep high turns the display on
  	digitalWrite(_DI0, LOW);       // Initialize data high
	
}

// Sends data to the display memory
ePaperDisplay::writeCharDisplay( char *charRowA, char *charRowB)
{
	// lets loop through our arrays and there character bit maps and write them to the device memory.
	// First Row
	for ( int i=0, i < 16, i++)
	{
		dataRowA[i] = dfltCharMap[charRowA[i]];
	}
	// Second Row
	for ( int i=0, i < 16, i++)
	{
		dataRowB[i] = dfltCharMap[charRowB[i]];
	}
	
	write(dataRowA, dataRowB);
}

ePaperDisplay::testPattern()
{
	dataRowA = { 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF};
	dataRowB = { 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF};
	write();
	delay(3000);
	invertDisplay = true;
	write();
	delay(3000);
	invertDisplay = false;
}
